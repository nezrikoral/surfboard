﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SurfboardStore.Data;
using SurfboardStore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace SurfboardStore.Controllers
{
    public class AccountController : Controller
    {
        private readonly SurfboardStoreContext _context;
      

        public AccountController(SurfboardStoreContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ViewResult Register()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Logout()
        {
            HttpContext.Session.Clear();            
            return RedirectToAction("Index", "Home");            
        }

        [HttpPost]
        public IActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_context.Users.Any(u => u.UserName == model.Username))
                {
                    
                    ModelState.AddModelError("Username", "User with this username already exists");
                    return View(model);
                }
                if (_context.Customers.Any(c => c.Email == model.Email))
                {

                    ModelState.AddModelError("Email", "User with this email already exists");
                    return View(model);
                }

                var user = new User {   UserName = model.Username, 
                                        Password = model.Password,
                                        UserRoleId = _context.UserRoles
                                                             .FirstOrDefault(ur => ur.Name == "Customer")
                                                             .Id
                };

                _context.Users.Add(user);
                _context.SaveChanges();


                var customer = new Customer
                {
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Phone = model.Phone,
                    Address = model.Address,
                    City = model.City,
                    Country = model.Country,
                    UserId = _context.Users.Single(u => u.UserName == model.Username).Id
                };

                _context.Customers.Add(customer);
                _context.SaveChanges();
            }

            return RedirectToAction("Login");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User objUser)
        {
            if (ModelState.IsValid)
            {

                var obj = _context.Users
                            .Include(u => u.UserRole)
                            .Where(a => a.UserName.Equals(objUser.UserName) && a.Password.Equals(objUser.Password)).FirstOrDefault();
                if (obj != null)
                {

                    HttpContext.Session.SetInt32("UserID", obj.Id);
                    HttpContext.Session.SetString("UserName", obj.UserName);
                    HttpContext.Session.SetInt32("IsAdmin", obj.UserRole.IsAdmin ? 1 : 0);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("UserName", "incorrect Usernname or Password.");
                }

            }
            return View(objUser);
        }

        public ActionResult Admin()
        {
            int? isAdmin = HttpContext.Session.GetInt32("IsAdmin");
            if (isAdmin != null && isAdmin == 1)
            {
                return View();
            }

            else
            {
                return RedirectToAction("Login");
            }
        }
    }
}
