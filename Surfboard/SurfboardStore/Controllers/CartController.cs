﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SurfboardStore.Data;
using SurfboardStore.Extensions;
using SurfboardStore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SurfboardStore.Controllers
{
    public class CartController : Controller
    {
        private readonly SurfboardStoreContext _context;
        public CartController(SurfboardStoreContext context)
        {
            _context = context;

        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ThankYou()
        {
            return View();
        }

        public ActionResult PlaceOrder()
        {
            List<Item> cart = HttpContext.Session.GetComplexData<List<Item>>("cart");
            int? userId = HttpContext.Session.GetInt32("UserID");

            if (cart == null || cart.Count == 0)
            {
                ModelState.AddModelError("Cart", "The Cart is Empty.");
                return RedirectToAction("Index", "Product");
            }
            if (userId == null)
            {
                ModelState.AddModelError("UserId", "No User is logged in.");
                return RedirectToAction("Login","Account");
            }
            else
            {
                var obj = new object();

                // lock for this step 
                lock (obj)
                {
                    // Check avilabilty quantity
                    foreach (var item in cart)
                    {
                        if (_context.Surfboard.Find(item.Surfboard.Id).Quantity < item.Quantity)
                        {
                            ModelState.AddModelError("Quantity", "No Enough quantity for surfboard quantity : " + item.Surfboard.Quantity);
                            return View("Index");
                        }
                    }

                    // create order 
                    var order = new Order
                    {
                        Date = DateTime.Now,
                        Status = Status.Placed,
                        UserId = (int)userId,
                        LocationId = _context.Locations.First().Id
                    };

                    _context.Orders.Add(order);
                    _context.SaveChanges();

                    int newOrderId = _context.Orders.Where(o => o.Date == order.Date &&
                                                        o.Status == order.Status &&
                                                        o.UserId == order.UserId).SingleOrDefault().Id;

                    foreach (var item in cart)
                    {
                        var bicycle = _context.Surfboard.Find(item.Surfboard.Id);
                        bicycle.Quantity -= item.Quantity;

                        _context.SurfboardOrders.Add(
                            new SurfboardOrder 
                            { 
                                SurfboardId = bicycle.Id, 
                                OrderID = newOrderId, 
                                SurfboardQuantity = item.Quantity 
                            });

                        _context.Surfboard.Update(bicycle);
                    }
                    _context.SaveChanges();
                }
            }
            HttpContext.Session.Remove("cart");

            // To Thank you page
            return RedirectToAction("ThankYou");
        }


        public ActionResult Add(int id, int quantity=1)
        {
            List<Item> cart = HttpContext.Session.GetComplexData<List<Item>>("cart");
            if (cart == null)
            {
                cart = new List<Item>();
                cart.Add(new Item { Surfboard = _context.Surfboard.Find(id), Quantity = quantity });
                HttpContext.Session.SetComplexData("cart", cart);
            }
            else
            {
                Item itemFromCart = cart.Find(i => i.Surfboard.Id == id);
                if (itemFromCart != null)
                {
                    itemFromCart.Quantity += quantity;
                }
                else
                {
                    cart.Add(new Item { Surfboard = _context.Surfboard.Find(id), Quantity = quantity });
                }
                HttpContext.Session.SetComplexData("cart", cart);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Remove(int id)
        {
            List<Item> cart = HttpContext.Session.GetComplexData<List<Item>>("cart");
            if(cart != null)
            {
                Item itemFromCart = cart.Find(i => i.Surfboard.Id == id);
                cart.Remove(itemFromCart);
                HttpContext.Session.SetComplexData("cart", cart);
            }

            return RedirectToAction("Index");
        }
    }
}
