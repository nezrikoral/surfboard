﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SurfboardStore.Data;
using SurfboardStore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace SurfboardStore.Controllers
{
    public class GraphController : Controller
    {
        private readonly SurfboardStoreContext _context;

        public GraphController(SurfboardStoreContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IEnumerable<GroupedResult> GetOrdersStatsByLocation()
        {
            //var res = _context.Orders.Include(o => o.Location)
            //                  .GroupBy(
            //            o => o.Location.Name)
            //                  .Select(group => new GroupedResult{
            //                      GroupName =  group.Key, 
            //                      Count = group.Count() })
            //                  .ToList();


            var a =(from order in _context.Orders
                    join location in _context.Locations
                    on order.LocationId equals location.Id
                    group order by location.Name into grouped
                    select new GroupedResult
                    {
                        GroupName = grouped.Key,
                        Count = grouped.Count()
                    }).ToList();
            return a;
        }

        [HttpGet]
        public IEnumerable<GroupedResult> GetSalesStatsBySurfboardColor()
        {
            var a =(from surfboardOrder in _context.SurfboardOrders
                    join surfboard in _context.Surfboard
                    on surfboardOrder.SurfboardId equals surfboard.Id
                    group surfboardOrder by surfboard.Color into grouped
                    select new GroupedResult
                    {
                        GroupName = grouped.Key,
                        Count = grouped.Count()
                    }).ToList();
            return a;
        }

        [HttpGet]
        public IEnumerable<GroupedResult> GetOrdersStatsByDate()
        {
            var a = (from order in _context.Orders
                    group order by order.Date.Date into grouped
                    orderby grouped.Key
                    select new GroupedResult
                    {
                        GroupName = grouped.Key.ToShortDateString(),
                        Count = grouped.Count()
                    })
                    .ToList();
            return a;
        }

    }
}
