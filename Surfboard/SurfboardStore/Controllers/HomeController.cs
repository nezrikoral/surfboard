﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SurfboardStore.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SurfboardStore.Models;
using Microsoft.EntityFrameworkCore;

namespace SurfboardStore.Controllers
{    
    public class HomeController : Controller
    {
        private readonly SurfboardStoreContext _context;

        public HomeController(SurfboardStoreContext context)
        {
            _context = context;
        }
        // GET: HomeController
        public async Task<IActionResult> Index()
        {
            return View(await _context.Surfboard.Where(b => b.Quantity > 0).Take(3).ToListAsync());
        }


        public ActionResult About()
        {
            return View();
        }


        public ActionResult Contact()
        {
            return View();
        }

        

    }
}
