﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SurfboardStore.Data;
using SurfboardStore.Models;
using Newtonsoft.Json.Serialization;

namespace SurfboardStore.Controllers
{
    public class ProductController : Controller
    {
        private readonly SurfboardStoreContext _context;

        public ProductController(SurfboardStoreContext context)
        {
            _context = context;
        }

        // GET: Surfboards
        public async Task<IActionResult> Index(string Color, string Type, int MinPrice, int MaxPrice)
        {
            var query = _context.Surfboard.Include(b => b.SurfboardType).AsQueryable();
            if (Color != null) 
            {
                query = query.Where(b => b.Color == Color);
            }
            if (Type != null)
            {
                query = query.Where(b => b.SurfboardType.Name == Type);
            }
            if (MinPrice != 0)
            {
                query = query.Where(b => b.Price >= MinPrice);

            }
             if( MaxPrice != 0)
            {
                query = query.Where(b => b.Price <= MaxPrice);
            }
            var result = await query.ToListAsync();
            return View(result);
        }



        // GET: Surfboards/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var surfboard = await _context.Surfboard
                .Include(b => b.SurfboardType)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (surfboard == null)
            {
                return NotFound();
            }

            return View(new Item(surfboard));
        }       

        
    }
}
