﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SurfboardStore.Data;
using SurfboardStore.Models;
using Microsoft.AspNetCore.Authorization;

namespace SurfboardStore.Controllers
{
    public class SurfboardController : Controller
    {
        private readonly SurfboardStoreContext _context;

        public SurfboardController(SurfboardStoreContext context)
        {
            _context = context;
        }

        // GET: Surfboard
        public async Task<IActionResult> Index()
        {
            var surfboardStoreContext = _context.Surfboard.Include(b => b.SurfboardType);
            return View(await surfboardStoreContext.ToListAsync());
        }

        // GET: Surfboard/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var surfboard = await _context.Surfboard
                .Include(b => b.SurfboardType)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (surfboard == null)
            {
                return NotFound();
            }

            return View(surfboard);
        }

        // GET: Surfboard/Create
        public IActionResult Create()
        {
            ViewData["SurfboardTypeId"] = new SelectList(_context.SurfboardType, "Id", "Id");
            return View();
        }

        // POST: Surfboard/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Price,Name,Color,Quantity,ImageUrl,SurfboardTypeId")] Surfboard surfboard)
        {
            if (ModelState.IsValid)
            {
                _context.Add(surfboard);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["SurfboardTypeId"] = new SelectList(_context.SurfboardType, "Id", "Id", surfboard.SurfboardType);
            return View(surfboard);
        }

        // GET: Surfboard/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var surfboard = await _context.Surfboard.FindAsync(id);
            if (surfboard == null)
            {
                return NotFound();
            }
            ViewData["SurfboardTypeId"] = new SelectList(_context.SurfboardType, "Id", "Id", surfboard.SurfboardType);
            return View(surfboard);
        }

        // POST: Surfboard/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Price,Name,Color,Quantity,ImageUrl,SurfboardTypeId")] Surfboard surfboard)
        {
            if (id != surfboard.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(surfboard);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SurfboardExists(surfboard.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["SurfboardTypeId"] = new SelectList(_context.SurfboardType, "Id", "Id", surfboard.SurfboardTypeId);
            return View(surfboard);
        }

        // GET: Surfboard/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var surfboard = await _context.Surfboard
                .Include(b => b.SurfboardType)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (surfboard == null)
            {
                return NotFound();
            }

            return View(surfboard);
        }

        // POST: Surfboard/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var surfboard = await _context.Surfboard.FindAsync(id);
            _context.Surfboard.Remove(surfboard);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SurfboardExists(int id)
        {
            return _context.Surfboard.Any(e => e.Id == id);
        }
    }
}
