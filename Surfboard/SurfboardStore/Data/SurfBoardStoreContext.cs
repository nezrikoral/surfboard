﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SurfboardStore.Models;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Web.CodeGeneration.EntityFrameworkCore;

namespace SurfboardStore.Data
{
    public class SurfboardStoreContext : DbContext
    {
        public DbSet<SurfboardStore.Models.Surfboard> Surfboard { get; set; }
        public DbSet<SurfboardType> SurfboardType { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<SurfboardOrder> SurfboardOrders { get; set; }

        public SurfboardStoreContext (DbContextOptions<SurfboardStoreContext> options)
            : base(options)
        {
            
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SurfboardOrder>()
                .HasKey(bo => new { bo.SurfboardId, bo.OrderID });

            modelBuilder.Entity<SurfboardOrder>()
                .HasOne(bo => bo.Order)
                .WithMany(o => o.SurfboardOrders)
                .HasForeignKey(bo => bo.OrderID);

            modelBuilder.Entity<SurfboardOrder>()
                .HasOne(bo => bo.Surfboard)
                .WithMany(o => o.SurfboardOrders)
                .HasForeignKey(bo => bo.SurfboardId);

            modelBuilder.Entity<SurfboardType>()
               .HasData
               (
                   new SurfboardType
                   {
                       Id = 1,
                       Name = "Ocean",
                       Description = "Best for surfing in the ocean."
                   },
                   new SurfboardType
                   {
                       Id = 2,
                       Name = "Long board",
                       Description = "Best for surfing in deep ocean waves."
                   },
                   new SurfboardType
                   {
                       Id = 3,
                       Name = "Soft",
                       Description = "Best for kids and beginers who learn to surf."
                   }
                   );

           modelBuilder.Entity<Surfboard>()
                .HasData
                (
                    new Surfboard
                    {
                        Id = 1,
                        Name = "Banana",
                        Color = "Yellow",
                        Quantity = 10,
                        Price = 199.99,
                        ImageUrl = "http://localhost:3000/images/products/yellow_surf.jpg",
                        SurfboardTypeId=1
                    },
                    new Surfboard
                    {
                        Id = 2,
                        Name = "Sky",
                        Color = "Blue",
                        Quantity = 10,
                        Price = 599.99,
                        ImageUrl = "http://localhost:3000/images/products/blue_surf.jpeg",
                        SurfboardTypeId = 3
                    },
                     new Surfboard
                     {
                         Id = 3,
                         Name = "Milk",
                         Color = "White",
                         Quantity = 10,
                         Price = 399.99,
                         ImageUrl = "http://localhost:3000/images/products/white_surf.jpg",
                         SurfboardTypeId = 2

                     },
                     new Surfboard
                     {
                         Id = 4,
                         Name = "Night",
                         Color = "Black",
                         Quantity = 10,
                         Price = 599.99,
                         ImageUrl = "http://localhost:3000/images/products/black_surf.jpg",
                         SurfboardTypeId = 3
                     },
                     new Surfboard
                     {
                         Id = 5,
                         Name = "Ocean",
                         Color = "Blue",
                         Quantity = 10,
                         Price = 199.99,
                         ImageUrl = "http://localhost:3000/images/products/blue_surf.jpg",
                         SurfboardTypeId = 1
                     },
                     new Surfboard
                     {
                         Id = 6,
                         Name = "Barni",
                         Color = "Purple",
                         Quantity = 10,
                         Price = 199.99,
                         ImageUrl = "http://localhost:3000/images/products/purple_surf.jpeg",
                         SurfboardTypeId = 1
                     }
                );
            modelBuilder.Entity<UserRole>()
                .HasData(
                    new UserRole
                    {
                        Id = 1,
                        Name = "Customer",
                        IsAdmin = false
                    },
                    new UserRole
                    {
                        Id = 2,
                        Name = "Developers",
                        IsAdmin = true
                    }
                );
            modelBuilder.Entity<User>()
                   .HasData(
                new User
                {
                    UserRoleId = 2,
                    UserName = "AdminAdmin",
                    Password = "123",
                    Id = 1
                },
                new User
                {
                    UserRoleId = 1,
                    UserName = "Ravid",
                    Password = "123",
                    Id = 2
                },
                new User
                {
                    UserRoleId = 1,
                    UserName = "Maya",
                    Password = "123",
                    Id = 3
                });

            modelBuilder.Entity<Location>()
                .HasData(
                    new Location
                    {
                        Id = 1,
                        Name = "Ramat HaSharon Warehouse",
                        City = "Ramat HaSharon",
                        Address = "Sokolov 71",
                        Country = "Israel"
                    }
                );

            modelBuilder.Entity<Customer>()
                .HasData(
                    new Customer
                    {
                        Id = 1,
                        FirstName = "Ravid",
                        LastName = "Tahel",
                        City = "Petah Tikva",
                        Country = "Israel",
                        Address = "Rotchild 26",
                        Email = "RavidTahel@gmail.com",
                        Phone = "+9725212345",
                        UserId = 2
                    }, 
                    new Customer
                    {
                        Id = 2,
                        FirstName = "Maya",
                        LastName = "Ester",
                        City = "Tel Aviv",
                        Country = "Israel",
                        Address = "Dizengoff 26",
                        Email = "MayaEster@gmail.com",
                        Phone = "+9725553235",
                        UserId = 3
                    }
                    );

            modelBuilder.Entity<Order>()
                .HasData(
                    new Order
                    {
                        Id = 1,
                        UserId = 2,
                        LocationId = 1,
                        Date = DateTime.Now,
                        Status = Status.Placed
                    },
                     new Order
                     {
                         Id = 2,
                         UserId = 3,
                         LocationId = 1,
                         Date = DateTime.Now,
                         Status = Status.Placed
                     }
                );

            modelBuilder.Entity<SurfboardOrder>()
                .HasData(
                    new SurfboardOrder { SurfboardId = 4, OrderID = 2 , SurfboardQuantity = 2},
                    new SurfboardOrder { SurfboardId = 5, OrderID = 2, SurfboardQuantity = 2 },
                    new SurfboardOrder { SurfboardId = 1, OrderID = 2 , SurfboardQuantity = 2},
                    new SurfboardOrder { SurfboardId = 1, OrderID = 1 , SurfboardQuantity = 1},
                    new SurfboardOrder { SurfboardId = 2, OrderID = 1 , SurfboardQuantity = 2},
                    new SurfboardOrder { SurfboardId = 3, OrderID = 1 , SurfboardQuantity = 2 }
                );
        }

    }
}
