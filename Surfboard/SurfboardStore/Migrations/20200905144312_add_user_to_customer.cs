﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SurfboardStore.Migrations
{
    public partial class add_user_to_customer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SurfboardType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurfboardType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    IsAdmin = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Surfboard",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Price = table.Column<double>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Color = table.Column<string>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    SurfboardTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Surfboard", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Surfboard_SurfboardType_SurfboardTypeId",
                        column: x => x.SurfboardTypeId,
                        principalTable: "SurfboardType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false),
                    UserRoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_UserRoles_UserRoleId",
                        column: x => x.UserRoleId,
                        principalTable: "UserRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    CustomerId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SurfboardOrders",
                columns: table => new
                {
                    SurfboardId = table.Column<int>(nullable: false),
                    OrderID = table.Column<int>(nullable: false),
                    SurfboardQuantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurfboardOrders", x => new { x.SurfboardId, x.OrderID });
                    table.ForeignKey(
                        name: "FK_SurfboardOrders_Surfboard_SurfboardId",
                        column: x => x.SurfboardId,
                        principalTable: "Surfboard",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SurfboardOrders_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "SurfboardType",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Best for surfing in the ocean.", "Ocean" },
                    { 2, "Best for surfing in deep ocean waves.", "Long board" },
                    { 3, "Best for kids and beginers who learn to surf.", "Soft" }
                });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "Id", "Address", "City", "Country", "Name" },
                values: new object[] { 1, "Sokolov 71", "Ramat HaSharon", "Israel", "Ramat HaSharon Warehouse" });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "Id", "IsAdmin", "Name" },
                values: new object[,]
                {
                    { 1, false, "Customer" },
                    { 2, true, "Developers" }
                });

            migrationBuilder.InsertData(
                table: "Surfboard",
                columns: new[] { "Id", "SurfboardTypeId", "Color", "ImageUrl", "Name", "Price", "Quantity" },
                values: new object[,]
                {
                    { 1, 1, "Yellow", "http://localhost:3000/images/products/yellow_surf.jpg", "Banana", 199.99000000000001, 10 },
                    { 5, 1, "Blue", "http://localhost:3000/images/products/blue_surf.jpg", "Ocean", 199.99000000000001, 10 },
                    { 6, 1, "Purple", "http://localhost:3000/images/products/purple_surf.jpeg", "Barni", 199.99000000000001, 10 },
                    { 3, 2, "White", "http://localhost:3000/images/products/white_surf.jpg", "Milk", 399.99000000000001, 10 },
                    { 2, 3, "Blue", "http://localhost:3000/images/products/blue_surf.jpeg", "Sky", 599.99000000000001, 10 },
                    { 4, 3, "Black", "http://localhost:3000/images/products/black_surf.jpg", "Night", 599.99000000000001, 10 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Password", "UserName", "UserRoleId" },
                values: new object[,]
                {
                    { 2, "123", "Ravid", 1 },
                    { 3, "123", "Maya", 1 },
                    { 1, "123", "AdminAdmin", 2 }
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Address", "City", "Country", "Email", "FirstName", "LastName", "Phone", "UserId" },
                values: new object[] { 1, "Rotchild 26", "Petah Tikva", "Israel", "RavidTahel@gmail.com", "Ravid", "Tahel", "+9725212345", 2 });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Address", "City", "Country", "Email", "FirstName", "LastName", "Phone", "UserId" },
                values: new object[] { 2, "Dizengoff 26", "Tel Aviv", "Israel", "MayaEster@gmail.com", "Maya", "Ester", "+9725553235", 3 });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "Id", "CustomerId", "Date", "LocationId", "Status" },
                values: new object[] { 1, 1, new DateTime(2020, 9, 5, 17, 43, 12, 347, DateTimeKind.Local).AddTicks(6253), 1, 0 });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "Id", "CustomerId", "Date", "LocationId", "Status" },
                values: new object[] { 2, 1, new DateTime(2020, 9, 5, 17, 43, 12, 352, DateTimeKind.Local).AddTicks(2715), 1, 0 });

            migrationBuilder.InsertData(
                table: "SurfboardOrders",
                columns: new[] { "SurfboardId", "OrderID", "SurfboardQuantity" },
                values: new object[,]
                {
                    { 1, 1, 1 },
                    { 2, 1, 2 },
                    { 3, 1, 2 },
                    { 4, 2, 2 },
                    { 5, 2, 2 },
                    { 1, 2, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Surfboard_SurfboardTypeId",
                table: "Surfboard",
                column: "SurfboardTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_SurfboardOrders_OrderID",
                table: "SurfboardOrders",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_UserId",
                table: "Customers",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_LocationId",
                table: "Orders",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserRoleId",
                table: "Users",
                column: "UserRoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SurfboardOrders");

            migrationBuilder.DropTable(
                name: "Surfboard");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "SurfboardType");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "UserRoles");
        }
    }
}
