﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SurfboardStore.Models
{
    public class Item
    {
        public Item()
        {

        }
            public Item(Surfboard surfboard)
        {
            this.Surfboard = surfboard;
            this.Quantity = 1;
        }
        public Surfboard Surfboard { get; set; }
        public int Quantity { get; set; }
    }
}
