﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SurfboardStore.Models
{
    public class Order
    {
        public int Id { get; set; }
        public Status Status { get; set; }
        public DateTime Date { get; set; }
        public int UserId { get; set; }
        public int LocationId { get; set; }
        public List<SurfboardOrder> SurfboardOrders { get; set; }
        public User User { get; set; }

        public Location Location { get; set; }
    }
}
