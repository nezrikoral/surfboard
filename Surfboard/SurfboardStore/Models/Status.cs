﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SurfboardStore.Models
{
    public enum Status
    {
        Placed,
        Shipped,
        Received
    }

}
