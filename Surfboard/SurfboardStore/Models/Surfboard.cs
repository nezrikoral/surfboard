﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SurfboardStore.Models
{
    public class Surfboard
    {
        public int Id { get; set; }
        [DataType(DataType.Currency)]
        public double Price { get; set; }
        [Required]
        public string Name { get; set; }
        public string Color { get; set; }
        public int Quantity { get; set; }

        [NotMapped]
        public bool InStock { get => this.Quantity > 0; }
        public string ImageUrl { get; set; }
        public int SurfboardTypeId { get; set; }
        public SurfboardType SurfboardType { get; set; }
        public List<SurfboardOrder> SurfboardOrders { get; set; }

    }
}
