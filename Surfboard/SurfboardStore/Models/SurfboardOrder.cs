﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SurfboardStore.Models
{
    public class SurfboardOrder
    {
        public int SurfboardId { get; set; }
        public int OrderID { get; set; }
        public int SurfboardQuantity { get; set; }

        public Order Order { get; set; }
        public Surfboard Surfboard { get; set; }
    }
}
