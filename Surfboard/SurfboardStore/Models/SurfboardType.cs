﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SurfboardStore.Models
{
    public class SurfboardType
    {
        public int Id { get; set; }
        [System.ComponentModel.DisplayName("Type")]
        public string Name { get; set; }
        public string Description { get; set; }

        public List<Surfboard> Surfboards { get; set; }
    }
}
