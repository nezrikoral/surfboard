﻿$(async function () {
    if (window.location.pathname == '/' ||
        window.location.pathname.toLowerCase() == '/home/index') {
        var app_key = "439d4b804bc8187953eb36d2a8c26a02";
        var tel_aviv_id = "293397";
        var url = "https://openweathermap.org/data/2.5/weather?id=" + tel_aviv_id + "&appid=" + app_key;
        var weather = (await $.ajax({
            type: "GET",
            url: url
        }));
        var rounded_tempature = Math.round(weather.main.temp);
        if (rounded_tempature >= 20) {
            $('p#weather-text').text("Today is great for a quick surf : " + rounded_tempature + " °C");
            var i = document.getElementById('weather-icon');
            i.setAttribute("class", "fas fa-sun");
            i.setAttribute("style", "color:yellow");
        }
        else {
            $('p#weather-text').text("Today is not great for a quick surf: " + rounded_tempature + " °C");
            var i = document.getElementById('weather-icon');
            i.setAttribute("class", "fas fa-cloud-sun");
            i.setAttribute("style", "color:blue");
        }
    }
});
