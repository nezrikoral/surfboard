﻿// graph.js
$(function () {
    if (window.location.pathname.toLowerCase() == '/graph/index') {
        $.ajax({
            url: '/Graph/GetSalesStatsBySurfboardColor',
            success: (data) => {
                createBarsGraph(data, "#sales-by-color-graph");
            }
        })

        $.ajax({
            url: '/Graph/GetOrdersStatsByDate',
            success: (data) => {
                createBarsGraph(data, "#orders-by-date-graph");
            }
        })

        $.ajax({
            url: '/Graph/GetOrdersStatsByLocation',
            success: (data) => {
                createPieGraph(data, "#orders-by-location-graph");
            }
        })
    }
})

function createPieGraph(data, id) {

    var width = 300,
        height = 300,
        radius = Math.min(width, height) / 2;

    var arc = d3.svg.arc()
        .outerRadius(radius - 10)
        .innerRadius(0);

    var pie = d3.layout.pie()
        .sort(null)
        .value(function (d) { return d.count; });

    var svg = d3.select(id).append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");


    var g = svg.selectAll(".arc")
        .data(pie(data))
        .enter().append("g")
        .attr("class", "arc");

    g.append("path")
        .attr("d", arc)
        .style("fill", randomBlueShade());

    g.append("text")
        .attr("transform", function (d) { return "translate(" + arc.centroid(d) + ")"; })
        .attr("dy", ".35em")
        .style("text-anchor", "middle")
        .text(function (d) { return d.data.groupName; });
}

function createBarsGraph(data, id) {

    var margin = { top: 20, right: 20, bottom: 70, left: 40 },
        width = 600,
        height = 300;

    // set the ranges
    var x = d3.scale.ordinal().rangeRoundBands([0, width], .05);
    var y = d3.scale.linear().range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .ticks(10);

    // add the SVG element
    var svg = d3.select(id).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");


    // scale the range of the data
    x.domain(data.map(function (d) { return d.groupName; }));
    y.domain([0, d3.max(data, function (d) { return d.count; })]);

    // add axis
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", "-.55em")
        .attr("transform", "rotate(-90)");

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 0)
        .attr("dy", "1em")
        .style("text-anchor", "end")
        .text("NumberOfComment");

    // Add bar chart
    svg.selectAll("bar")
        .data(data)
        .enter().append("rect")
        .attr("class", "bar")
        .attr("x", function (d) { return x(d.groupName) + 10; })
        .attr("width", x.rangeBand() / 2)
        .attr("y", function (d) { return y(d.count); })
        .attr("height", function (d) { return height - y(d.count); })
        .attr("fill", function (d) { return randomBlueShade() });
}

function randomBlueShade() {
    var h = 240;
    var s = Math.floor(Math.random() * 100);
    var l = Math.floor(Math.random() * 100);
    return 'hsl(' + h + ', ' + s + '%, ' + l + '%)';
}